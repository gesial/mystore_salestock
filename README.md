---

## Bug Report

1. "Bug Documentation" consist of bug report list and each bug video for proof
2. "Bug Documentation > Bug Report - Web - My Store.xlsx" is the bug report list
3. "Bug Repro Automation > cypress > integration > BugRepro.js" is the automation code for bug reproducing using cypress

---

## Performance

1. "Test Plan.jmx" is the Apache Jmeter file
2. "Performance.png" is the graph result screen shot

---

## Test Case
1. "Test Case - Web - My Store.xlsx" is the Test Case for My Store

---

## UI_automation
1. "UI_automate__MyStore.mp4" is the video of the automation running
2. "cypress > integration > MyStore.js" is the automation code for UI automation using cypress



