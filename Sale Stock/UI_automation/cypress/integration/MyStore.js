const urlHome = 'http://automationpractice.com'

// Constant to make every email for registering is different each time the test run
const tgl = new Date();
const email = 'verif' + tgl.getDate() + tgl.getHours() + tgl.getMinutes() + '@mailnesia.com';

// Authentication Page Scenarios
describe('Authentication Page', function () {
	// Repeat before every scenario inside
	beforeEach(function() {
		// Go to authentication 
		cy.visit(urlHome)
		cy.get('.login').click()
	})
	
	it('AT-1 Register', function() {
		cy.get('#email_create').type(email)
		cy.get('#SubmitCreate').click()
		
		// Fill out personal information
		cy.get('#id_gender1').check()
		cy.get('#customer_firstname').type('Gesi')
		cy.get('#customer_lastname').type('Test')
		cy.get('#passwd').type('qwe123')
		cy.get('#days').select('11').should('have.value', '11')
		cy.get('#months').select('March').should('have.value', '3')
		cy.get('#years').select('1995').should('have.value', '1995')
		
		// Checklisting newsletter and special offering
		cy.get('#newsletter').check()
		cy.get('#optin').check()
		
		// Fill out address
		cy.get('#firstname').clear().type('Ibu Kos')
		cy.get('#lastname').clear().type('Bumi Sakinah')
		cy.get('#company').type('My Store')
		cy.get('#address1').type('Semanggi')
		cy.get('#address2').type('Jakarta')
		cy.get('#city').type('Jakarta Selatan')
		cy.get('#id_country').select('United States').should('have.value', '21')
		cy.get('#id_state').select('Connecticut').should('have.value', '7')
		cy.get('#postcode').type('12930')
		cy.get('#other').type('Lorem ipsum dolor sit amet')
		cy.get('#phone').type('123456')
		cy.get('#phone_mobile').type('081234567890')
		cy.get('#alias').clear().type('Kantor')
		
		cy.get('#submitAccount').click()
		
		// Assertions - should be in the My Account page
		cy.get('.page-heading').contains('My account').should('be.visible')
	})

	it('AT-2 Log in', function() {
		// Fill out email and password
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		
		// Assertions - should have a username in the navbar
		cy.get('.account').contains('Gesi Test').should('be.visible')
	})
	
	it('AT-3 Log out', function() {
		// Log in
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		
		// Log out
		cy.get('.logout').click()
		
		// Assertions - should have a sign in button in the navbar
		cy.get('.login').should('be.visible')
	})
	
	it('AT-4 Register - email already registered', function() {
		cy.get('#email_create').type('ss_gesi@mailinator.com')
		cy.get('#SubmitCreate').click()
		
		// Assertions - should have an error notice
		cy.get('#create_account_error').contains('An account using this email address has already been registered. Please enter a valid password or request a new one. ').should('be.visible')
	})
	
	it('AT-5 Register - invalid email', function() {
		cy.get('#email_create').type('test@')
		cy.get('#SubmitCreate').click()
		
		// Assertions - should have an error notice
		cy.get('#create_account_error').contains('Invalid email address.').should('be.visible')
	})
	
	it('AT-6 Register - email field empty', function() {
		cy.get('#SubmitCreate').click()
		
		// Assertions - should have an error notice
		cy.get('#create_account_error').contains('Invalid email address.').should('be.visible')
	})

	it('AT-7 Log in - email not registered', function() {
		// Fill out email and password
		cy.get('#email').type('notreg_gesi@mailinator.com')
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		
		// Assertions - should have an error notice
		cy.get('.alert.alert-danger').contains('Authentication failed.').should('be.visible')
	})
	
	it('AT-8 Log in - wrong password', function() {
		// Fill out email and password
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#passwd').type('qwe')
		cy.get('#SubmitLogin').click()
		
		// Assertions - should have an error notice
		cy.get('.alert.alert-danger').contains('Invalid password.').should('be.visible')
	})
	
	it('AT-9 Log in - email field empty', function() {
		// Fill out password
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		
		// Assertions - should have an error notice
		cy.get('.alert.alert-danger').contains('An email address required.').should('be.visible')
	})
	
	it('AT-10 Log in - pasword field empty', function() {
		// Fill out email
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#SubmitLogin').click()
		
		// Assertions - should have an error notice
		cy.get('.alert.alert-danger').contains('Password is required.').should('be.visible')
	})

})

// Shopping Process Scenarios
describe('Shopping Process', function () {
	// Repeat before every scenario inside
	beforeEach(function() {
		cy.visit(urlHome)
	})
	
	it('SP-1 Purchasing item - login', function() {
		// Login
		cy.get('.login').click()
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		
		// Assertions - should have a username in the navbar
		cy.get('.account').contains('Gesi Test').should('be.visible')
		
		// Purchase an item
		cy.get('.logo.img-responsive').click()
		
		// Open Women category
		cy.get('.sf-with-ul[title=Women]').click()
		
		// Hover an item, add to cart, proceed to checkout
		cy.get('img[title=Blouse]').click()
		cy.get('button.exclusive').click()
		cy.get('.btn.btn-default.button.button-medium').contains('Proceed to checkout').click()
		
		// Checkout process
		cy.get('.button.btn.btn-default.standard-checkout.button-medium').contains('Proceed to checkout').click()
		cy.get('button[name=processAddress]').click()
		cy.get('#cgv').check()
		cy.get('.button.btn.btn-default.standard-checkout.button-medium').contains('Proceed to checkout').click()
		cy.get('a.bankwire').click()
		cy.get('.button.btn.btn-default.button-medium').contains('I confirm my order').click()
		
		// Assertions - order complete notification is visible
		cy.get('strong.dark').contains('Your order on My Store is complete.').should('be.visible')
	})
	
	it('SP-2 Purchasing item - not login', function() {
		// Purchase an item
		cy.get('.logo.img-responsive').click()
		
		// Open Women category
		cy.get('.sf-with-ul[title=Women]').click()
		
		// Hover an item, add to cart, proceed to checkout
		cy.get('img[title=Blouse]').click()
		cy.get('button.exclusive').click()
		cy.get('.btn.btn-default.button.button-medium').contains('Proceed to checkout').click()
		
		// Checkout process
		cy.get('.button.btn.btn-default.standard-checkout.button-medium').contains('Proceed to checkout').click()
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		cy.get('button[name=processAddress]').click()
		cy.get('#cgv').check()
		cy.get('.button.btn.btn-default.standard-checkout.button-medium').contains('Proceed to checkout').click()
		cy.get('a.bankwire').click()
		cy.get('.button.btn.btn-default.button-medium').contains('I confirm my order').click()
		
		// Assertions - order complete notification is visible
		cy.get('strong.dark').contains('Your order on My Store is complete.').should('be.visible')
	})
	
	it('SP-3 Purchasing item - unchecklist shipping Terms of service', function() {
		// Login
		cy.get('.login').click()
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		
		// Assertions - should have a username in the navbar
		cy.get('.account').contains('Gesi Test').should('be.visible')
		
		// Purchase an item
		cy.get('.logo.img-responsive').click()
		
		// Open Women category
		cy.get('.sf-with-ul[title=Women]').click()
		
		// Hover an item, add to cart, proceed to checkout
		cy.get('img[title=Blouse]').click()
		cy.get('button.exclusive').click()
		cy.get('.btn.btn-default.button.button-medium').contains('Proceed to checkout').click()
		
		// Checkout process
		cy.get('.button.btn.btn-default.standard-checkout.button-medium').contains('Proceed to checkout').click()
		cy.get('button[name=processAddress]').click()
		cy.get('.button.btn.btn-default.standard-checkout.button-medium').contains('Proceed to checkout').click()
		
		// Assertions - pop up notification occured
		cy.get('.fancybox-error').contains('You must agree to the terms of service before continuing.').should('be.visible')
	})
})