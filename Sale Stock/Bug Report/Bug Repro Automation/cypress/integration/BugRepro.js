const urlHome = 'http://automationpractice.com'

// Bug Reproduction
describe('Bug Reproduction', function () {
	// Repeat before every scenario inside
	beforeEach(function() {
		// Go to authentication 
		cy.visit(urlHome)
	})
	
	it('BUG-4 Cart item is reset after logout', function() {
		// Login
		cy.get('.login').click()
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		
		// Assertions - should have a username in the navbar
		cy.get('.account').contains('Gesi Test').should('be.visible')
		
		// Open Women category
		cy.get('.sf-with-ul[title=Women]').click()
		
		// Hover an item, add to cart, proceed to checkout
		cy.get('img[title=Blouse]').click()
		cy.get('button.exclusive').click()
		cy.get('.continue.btn.btn-default.button.exclusive-medium').click()
		
		// Asssertions - Cart has an item
		cy.get('a[title="View my shopping cart"]').trigger('mouseover')
		cy.get('.cart_block_product_name').contains('Blouse').should('be.visible')
		
		// Log out
		cy.get('.logout').click()
		
		// Assertions - should have a sign in button in the navbar
		cy.get('.login').should('be.visible')
		
		// Login
		cy.get('.login').click()
		cy.get('#email').type('ss_gesi@mailinator.com')
		cy.get('#passwd').type('qwe123')
		cy.get('#SubmitLogin').click()
		
		// Asssertions - Cart should have an item
		cy.get('a[title="View my shopping cart"]').trigger('mouseover')
		cy.get('.cart_block_product_name').contains('Blouse').should('be.visible') // The test is FAILED here
	})
	
	it('BUG-5 Sorting by highest to lowest price is incorrect', function() {
		// Open Women category
		cy.get('.logo.img-responsive').click()
		cy.get('.sf-with-ul[title=Women]').click()
		
		// Sort by highest price first
		cy.get('#selectProductSort').select('Price: Highest first').should('have.value', 'price:desc')
		cy.get('.price.product-price').eq(1).contains('50.99') // The test is FAILED here
	})
	
	it('BUG-6 Sorting by name from Z to A is incorrect', function() {
		// Open Women category
		cy.get('.logo.img-responsive').click()
		cy.get('.sf-with-ul[title=Women]').click()
		
		// Sort by highest price first
		cy.get('#selectProductSort').select('Product Name: Z to A').should('have.value', 'name:desc')
		cy.get('h5[itemprop=name]').eq(0).contains('Printed Summer Dress') // The test is FAILED here
	})
})